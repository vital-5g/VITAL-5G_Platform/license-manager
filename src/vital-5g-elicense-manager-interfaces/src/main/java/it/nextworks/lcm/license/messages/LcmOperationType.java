package it.nextworks.lcm.license.messages;

public enum LcmOperationType {
    NETWORK_APPLICATION_INSTANTIATION,
    NETWORK_APPLICATION_TERMINATION
}
