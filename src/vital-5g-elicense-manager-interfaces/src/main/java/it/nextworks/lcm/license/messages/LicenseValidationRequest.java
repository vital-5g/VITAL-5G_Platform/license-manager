package it.nextworks.lcm.license.messages;

import java.util.UUID;

public class LicenseValidationRequest {

    private LcmOperationType operationType;
    private String licenseKey;
    private UUID serviceInstanceId;


    public LicenseValidationRequest() {
    }

    public LicenseValidationRequest(LcmOperationType operationType, String licenseKey, UUID serviceInstanceId) {
        this.operationType = operationType;
        this.licenseKey = licenseKey;
        this.serviceInstanceId = serviceInstanceId;
    }


    public LcmOperationType getOperationType() {
        return operationType;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public UUID getServiceInstanceId() {
        return serviceInstanceId;
    }
}
