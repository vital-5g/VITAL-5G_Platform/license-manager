package it.nextworks.lcm.license.messages;

import java.util.UUID;

public class LicenseValidationResult {

    private UUID licenseId;
    private boolean successful;


    public LicenseValidationResult(UUID licenseId, boolean successful) {
        this.licenseId = licenseId;
        this.successful = successful;
    }


    public UUID getLicenseId() {
        return licenseId;
    }

    public boolean isSuccessful() {
        return successful;
    }
}
