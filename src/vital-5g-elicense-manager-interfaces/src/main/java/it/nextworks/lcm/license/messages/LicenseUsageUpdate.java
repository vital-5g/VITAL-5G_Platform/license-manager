package it.nextworks.lcm.license.messages;

import java.util.UUID;

public class LicenseUsageUpdate {

    private UUID serviceInstanceId;

    private String licenseKey;

    private LcmOperationType lcmOperationType;


    public LicenseUsageUpdate(UUID serviceInstanceId, LcmOperationType lcmOperationType, String licenseKey) {
        this.serviceInstanceId = serviceInstanceId;
        this.lcmOperationType = lcmOperationType;
        this.licenseKey = licenseKey;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public LicenseUsageUpdate() {
    }

    public UUID getServiceInstanceId() {
        return serviceInstanceId;
    }

    public LcmOperationType getLcmOperationType() {
        return lcmOperationType;
    }
}
