package it.nextworks.lcm.license.interfaces;

import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.lcm.license.messages.LicenseUsageUpdate;
import it.nextworks.lcm.license.messages.LicenseValidationRequest;
import it.nextworks.lcm.license.messages.LicenseValidationResult;

import java.util.UUID;

public interface LicenseValidationInterface {

    public LicenseValidationResult validateLicense(UUID networkApplicationId, LicenseValidationRequest request) throws NotExistingEntityException, FailedOperationException;
    public void updateLicenseUsage(UUID licenseId, LicenseUsageUpdate licenseUsageUpdate) throws NotExistingEntityException, FailedOperationException, MalformattedElementException;


}
