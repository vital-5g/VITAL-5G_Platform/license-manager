package it.nextworks.catalogue.license.elements;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import it.nextworks.catalogue.elements.LicenseType;
import it.nextworks.catalogue.elements.RuntimeDiagnosticAction;
import it.nextworks.catalogue.elements.RuntimeThresholdAction;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.util.UUID;

@Entity

public class InstanceBasedLicense  extends License{

    private int maxInstances;

    private int currentInstanceCount;


    public InstanceBasedLicense() {
    }

    public InstanceBasedLicense(UUID netAppBlueprintId, String softwareLicenseId, String secretKey, int maxInstances,  LicenseInfo licenseInfo) {
        super(netAppBlueprintId, softwareLicenseId, LicenseType.INSTANCE_BASED, secretKey, licenseInfo);
        this.maxInstances = maxInstances;
        this.currentInstanceCount = 0;
    }

    public int getMaxInstances() {
        return maxInstances;
    }

    public int getCurrentInstanceCount() {
        return currentInstanceCount;
    }

    public void setCurrentInstanceCount(int currentInstanceCount) {
        this.currentInstanceCount = currentInstanceCount;
    }
}
