package it.nextworks.catalogue.license.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RuntimeThresholdAction.class, name = "INSTANCE_BASED"),
        @JsonSubTypes.Type(value = RuntimeDiagnosticAction.class, name = "FLAT"),

})
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class License implements DescriptorInformationElement {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;

    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
    @JoinColumn(name="license_info")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private LicenseInfo  licenseInfo;


    private UUID netAppBlueprintId;
    private String softwareLicenseId;
    private LicenseType type;

    private String secretKey;



    public License() {


    }

    public License(UUID netAppBlueprintId,
                   String softwareLicenseId,
                   LicenseType type,
                   String secretKey,
                   LicenseInfo licenseInfo) {
        this.softwareLicenseId = softwareLicenseId;
        this.secretKey=secretKey;
        this.type = type;
        this.licenseInfo= licenseInfo;
        this.netAppBlueprintId = netAppBlueprintId;
    }


    public LicenseInfo getLicenseInfo() {
        return licenseInfo;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getSoftwareLicenseId() {
        return softwareLicenseId;
    }



    public LicenseType getType() {
        return type;
    }

    public UUID getNetAppBlueprintId() {
        return netAppBlueprintId;
    }

    public void isValid() throws MalformattedElementException {
    }
}
