package it.nextworks.catalogue.license.interfaces;

import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.license.elements.License;
import it.nextworks.catalogue.license.elements.LicenseInfo;

import java.util.UUID;

public interface LicenseCatalogueManagement {

    public UUID createLicense(UUID netappBlueprintId, SoftwareLicense license) throws NotExistingEntityException, MalformattedElementException, FailedOperationException;

    public License getLicense(UUID licenseId) throws NotExistingEntityException, FailedOperationException;

    public void deleteLicense(UUID licenseId, boolean forced) throws NotExistingEntityException, FailedOperationException;

    public void deleteLicenseForNetApp(UUID netappBlueprintId) throws NotExistingEntityException, FailedOperationException;

}
