package it.nextworks.catalogue.license.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.nextworks.catalogue.elements.SoftwareLicense;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class LicenseInfo {

    @Id
    @GeneratedValue
    private UUID licenseId;


    private UUID netappBlueprintId;

    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "licenseInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private License license;

    private String owner;


    @ElementCollection
    private List<UUID> serviceInstances = new ArrayList<>();


    public LicenseInfo() {
    }

    public LicenseInfo(License license, String owner, UUID netappBlueprintId) {
        this.license = license;
        this.owner = owner;
        this.netappBlueprintId = netappBlueprintId;
    }

    public List<UUID> getServiceInstances() {
        return serviceInstances;
    }

    public UUID getLicenseId() {
        return licenseId;
    }

    public UUID getNetappBlueprintId() {
        return netappBlueprintId;
    }

    public License getLicense() {
        return license;
    }

    public String getOwner() {
        return owner;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public void addServiceInstance(UUID serviceInstanceId){
        serviceInstances.add(serviceInstanceId);
    }


    public void removeServiceInstance(UUID serviceInstanceId){
        serviceInstances.remove(serviceInstanceId);
    }
}
