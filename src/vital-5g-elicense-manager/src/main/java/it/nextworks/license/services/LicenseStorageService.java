package it.nextworks.license.services;

import it.nextworks.catalogue.elements.LicenseType;
import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.license.elements.InstanceBasedLicense;
import it.nextworks.catalogue.license.elements.License;
import it.nextworks.catalogue.license.elements.LicenseInfo;
import it.nextworks.catalogue.license.interfaces.LicenseCatalogueManagement;
import it.nextworks.license.repo.LicenseInfoRepository;
import it.nextworks.license.repo.LicenseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class LicenseStorageService implements LicenseCatalogueManagement {


    private static final Logger log = LoggerFactory.getLogger(LicenseStorageService.class);
    @Autowired
    private LicenseInfoRepository licenseInfoRepository;
    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private SecurityService securityService;

    @Override
    public UUID createLicense(UUID netappBlueprintId, SoftwareLicense license) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request to create license");

        String owner = securityService.getUsername();
        LicenseInfo licenseInfo = new LicenseInfo(null, owner, netappBlueprintId );
        licenseInfoRepository.saveAndFlush(licenseInfo);
        log.debug("Stored license info with ID: {}", licenseInfo.getLicenseId());
        if(license.getType().equals(LicenseType.INSTANCE_BASED)&&!license.getLicenseParams().containsKey("KEY"))
            throw new MalformattedElementException("Instance Based License without KEY param");
        String secretKey = license.getLicenseParams().get("KEY");
        License nLicense = null;
        if(license.getType().equals(LicenseType.INSTANCE_BASED)){
            if(!license.getLicenseParams().containsKey("MAX_INSTANCES"))
                throw new MalformattedElementException("Instance Based license without MAX_INSTANCES param");
            int maxInstances =  Integer.parseInt(license.getLicenseParams().get("MAX_INSTANCES"));
            nLicense= new InstanceBasedLicense(netappBlueprintId, license.getSoftwareLicenseId(), secretKey, maxInstances,licenseInfo);
        }
        licenseInfo.setLicense(nLicense);
        licenseInfoRepository.saveAndFlush(licenseInfo);
        //licenseRepository.saveAndFlush(nLicense);
        log.debug("Stored license");
        return licenseInfo.getLicenseId();
    }

    @Override
    public License getLicense(UUID licenseId) throws NotExistingEntityException, FailedOperationException {
        log.debug("Received request to retrieve license with ID {}", licenseId);
        Optional<LicenseInfo>  licenseInfoO = licenseInfoRepository.findById(licenseId);
        if(licenseInfoO.isPresent())
        {
            return licenseInfoO.get().getLicense();
        }else{
            log.error("Could not find license with ID:{}", licenseId);
            throw new NotExistingEntityException("Could not find license with ID:"+licenseId);
        }

    }

    @Override
    public void deleteLicense(UUID licenseId, boolean forced) throws NotExistingEntityException, FailedOperationException {
        log.debug("Received request to delete license with ID {}", licenseId);
        Optional<LicenseInfo>  licenseInfoO = licenseInfoRepository.findById(licenseId);
        if(licenseInfoO.isPresent())
        {
            if(!licenseInfoO.get().getServiceInstances().isEmpty() && !forced){
                throw new FailedOperationException("License currently in use by:"+licenseInfoO.get().getServiceInstances());
            }
            licenseRepository.delete(licenseInfoO.get().getLicense());
            licenseInfoRepository.deleteById(licenseId);


        }else{
            log.error("Could not find license with ID:{}", licenseId);
            throw new NotExistingEntityException("Could not find license with ID:"+licenseId);
        }
    }

    @Override
    public void deleteLicenseForNetApp(UUID netappBlueprintId) throws NotExistingEntityException, FailedOperationException {

    }
}
