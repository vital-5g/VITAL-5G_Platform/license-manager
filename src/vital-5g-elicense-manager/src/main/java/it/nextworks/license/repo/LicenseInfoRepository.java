package it.nextworks.license.repo;

import it.nextworks.catalogue.license.elements.LicenseInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LicenseInfoRepository extends JpaRepository<LicenseInfo, UUID> {

    List<LicenseInfo> findAllByNetappBlueprintId(UUID netappBlueprintId);

}
