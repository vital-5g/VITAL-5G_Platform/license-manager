package it.nextworks.license.services;

import it.nextworks.catalogue.elements.LicenseType;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.license.elements.InstanceBasedLicense;
import it.nextworks.catalogue.license.elements.LicenseInfo;
import it.nextworks.lcm.license.interfaces.LicenseValidationInterface;
import it.nextworks.lcm.license.messages.LcmOperationType;
import it.nextworks.lcm.license.messages.LicenseUsageUpdate;
import it.nextworks.lcm.license.messages.LicenseValidationRequest;
import it.nextworks.lcm.license.messages.LicenseValidationResult;
import it.nextworks.license.repo.LicenseInfoRepository;
import it.nextworks.license.repo.LicenseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LicenseValidationService implements LicenseValidationInterface {

    private static final Logger log = LoggerFactory.getLogger(LicenseValidationService.class);

    @Autowired
    private LicenseInfoRepository licenseInfoRepository;
    @Autowired
    private LicenseRepository licenseRepository;

    @Autowired
    private SecurityService securityService;


    @Override
    public LicenseValidationResult validateLicense(UUID networkApplicationId, LicenseValidationRequest request) throws NotExistingEntityException, FailedOperationException {
        log.debug("Received request to validate license for NetApp:{}", networkApplicationId);
        List<LicenseInfo> allLicenseInfos = licenseInfoRepository.findAll();
        List<LicenseInfo> licenseInfos = allLicenseInfos.stream()
                .filter(lI -> lI.getNetappBlueprintId().equals(networkApplicationId))
                .collect(Collectors.toList());
        if(!licenseInfos.isEmpty()){
            LicenseValidationResult result = new LicenseValidationResult(null, true);
            for(LicenseInfo info : licenseInfos){

                if(info.getLicense().getType().equals(LicenseType.INSTANCE_BASED)){
                    if(info.getLicense().getSecretKey().equals(request.getLicenseKey())){
                        log.debug("Found matching key:{}", info.getLicenseId());
                        InstanceBasedLicense iLicense = (InstanceBasedLicense)info.getLicense();
                        if(info.getServiceInstances().size()<iLicense.getMaxInstances()){
                            return new LicenseValidationResult(info.getLicenseId(), true);
                        }else{
                            log.debug("Exceeded number of instances");
                            return new LicenseValidationResult(info.getLicenseId(), false);
                        }
                    }
                }

            }
            log.debug("Could not find license");
            return result;
        }else throw new NotExistingEntityException("Could not find licenses for NetApp:"+networkApplicationId);

    }

    @Override
    public void updateLicenseUsage(UUID licenseId, LicenseUsageUpdate licenseUsageUpdate) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request to update license usage:{}", licenseId);
        if(licenseUsageUpdate.getLicenseKey()==null){
            throw  new MalformattedElementException("License Update without license key");
        }else  if(licenseUsageUpdate.getLcmOperationType()==null) {
            throw  new MalformattedElementException("License Update without operation type");
        }else  if(licenseUsageUpdate.getServiceInstanceId()==null) {
            throw  new MalformattedElementException("License Update without instance id");
        }
        Optional<LicenseInfo> licenseInfoOptional = licenseInfoRepository.findById(licenseId);
        if(licenseInfoOptional.isPresent()){
            LicenseInfo info = licenseInfoOptional.get();
            if(!info.getLicense().getSecretKey().equals(licenseUsageUpdate.getLicenseKey())){
                throw new FailedOperationException("License key not valid");
            }

            if(licenseUsageUpdate.getLcmOperationType().equals(LcmOperationType.NETWORK_APPLICATION_INSTANTIATION)){
                log.debug("Adding service instance to license {}", licenseUsageUpdate.getServiceInstanceId());
                info.addServiceInstance(licenseUsageUpdate.getServiceInstanceId());
            }else if(licenseUsageUpdate.getLcmOperationType().equals(LcmOperationType.NETWORK_APPLICATION_TERMINATION)){
                log.debug("Removing service instance from license {}", licenseUsageUpdate.getServiceInstanceId());
                info.removeServiceInstance(licenseUsageUpdate.getServiceInstanceId());
            }
            licenseInfoRepository.saveAndFlush(info);
        }else throw new NotExistingEntityException("Could NOT find license with ID:"+licenseId);
    }
}
