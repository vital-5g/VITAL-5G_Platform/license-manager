package it.nextworks.license.repo;

import it.nextworks.catalogue.license.elements.License;
import it.nextworks.catalogue.license.elements.LicenseInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseRepository extends JpaRepository<License, Long> {

}
