package it.nextworks.license.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.license.elements.License;
import it.nextworks.catalogue.license.interfaces.LicenseCatalogueManagement;
import it.nextworks.lcm.license.interfaces.LicenseValidationInterface;
import it.nextworks.lcm.license.messages.LicenseUsageUpdate;
import it.nextworks.lcm.license.messages.LicenseValidationRequest;
import it.nextworks.lcm.license.messages.LicenseValidationResult;
import it.nextworks.license.services.LicenseStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Api(tags = "VITAL-5G Network Application License Manager API")
@RestController
@CrossOrigin
@RequestMapping("/portal/license/")

public class LicenseRestController {

    private static final Logger log = LoggerFactory.getLogger(LicenseRestController.class);
    @Autowired
    private LicenseCatalogueManagement licenseCatalogueManagement;


    @Autowired
    private LicenseValidationInterface licenseValidationInterface;


    @ApiOperation(value = "Create Network Application License")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "License onboarded", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element NOT found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"","/"}, method = RequestMethod.POST)
    public ResponseEntity<?> createLicense(@RequestParam UUID netappBlueprintId, @RequestBody SoftwareLicense license) {
        log.debug("Received request to Create Network Application License.");
        try {

            UUID licenseId = licenseCatalogueManagement.createLicense(netappBlueprintId, license);
            return new ResponseEntity<UUID>( licenseId, HttpStatus.CREATED);
        } catch (NotExistingEntityException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

        } catch (FailedOperationException e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (MalformattedElementException e) {
            log.error("Malformatted element exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }



    @ApiOperation(value = "Validate Network Application License")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License validation result", response = LicenseValidationResult.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element NOT found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/validate/{networkAppId}"}, method = RequestMethod.POST)
    public ResponseEntity<?> validateLicense(@PathVariable UUID networkAppId, @RequestBody LicenseValidationRequest request) {
        log.debug("Received request to Validate Network Application License.");
        try {

            LicenseValidationResult result = licenseValidationInterface.validateLicense(networkAppId, request);
            return new ResponseEntity<LicenseValidationResult>( result, HttpStatus.OK);
        } catch (NotExistingEntityException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

        } catch (FailedOperationException e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Use Network Application License")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License usage result"),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element NOT found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/use/{licenseId}"}, method = RequestMethod.POST)
    public ResponseEntity<?> updateLicense(@PathVariable UUID licenseId, @RequestBody LicenseUsageUpdate request) {
        log.debug("Received request to Validate Network Application License.");
        try {

            licenseValidationInterface.updateLicenseUsage(licenseId, request);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NotExistingEntityException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

        } catch (FailedOperationException e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }catch (MalformattedElementException e) {
            log.error("Malformatted element exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(value = "Use Network Application License")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "License usage result"),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 403, message = "Unauthorized request", response = ResponseEntity.class),
            @ApiResponse(code = 404, message = "Element NOT found", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/{licenseId}"}, method = RequestMethod.POST)
    public ResponseEntity<?> deleteLicense(@PathVariable UUID licenseId, @RequestParam boolean forced) {
        log.debug("Received request to delete Network Application License.");
        try {

            licenseCatalogueManagement.deleteLicense(licenseId, forced);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NotExistingEntityException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

        } catch (FailedOperationException e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
